// IIFE with jQuery Wrapper
(function($) {
  'use strict';

  /*
   *----------------------------------
   * Document Ready
   *----------------------------------
   */
	$(document).ready(function() {

    /****************************
      Home Slider Top
    *****************************/
    $('.slider_main').owlCarousel({
      loop:true,
      margin:0,
      nav:true,
      dots: false,
      items:1,
      smartSpeed:2500
    })  

    /****************************
      Freature Property Slides
    *****************************/

    $('.freature_carousel').owlCarousel({
      loop:true,
      margin:25,
      nav:true,
      dots: false,
      smartSpeed:2500,
      autoplay:true,
      autoplayTimeout:6000,
      autoplayHoverPause:false,
      responsiveClass:true,
      responsive:{
          0:{
              items:1,
          },
          600:{
              items:1,
          },
          1000:{
              items:4,
          }
      }

    })

    /****************************
      2nd Freature Property Slides
    *****************************/

    $('.freature_carousel_2').owlCarousel({
      loop:true,
      margin:25,
      nav:true,
      dots: false,
      smartSpeed:2500,
      autoplay:true,
      autoplayTimeout:6000,
      autoplayHoverPause:false,
      responsiveClass:true,
      responsive:{
          0:{
              items:1,
          },
          600:{
              items:1,
          },
          1000:{
              items:2,
          }
      }

    })

    /****************************
      Review Slides
    *****************************/

    $('.review_slider').owlCarousel({
      loop:true,
      margin:25,
      nav:false,
      dots: true,
      smartSpeed:2500,
      autoplay:true,
      autoplayTimeout:5000,
      autoplayHoverPause:false,
      responsiveClass:true,
      responsive:{
          0:{
              items:1,
          },
          600:{
              items:1,
          },
          1000:{
              items:3,
          }
      }

    })

    /****************************
      product_gallary Slides
    *****************************/

    $('.product_gallary').owlCarousel({
      loop:true,
      margin:25,
      nav:false,
      dots: true,
      smartSpeed:5000,
      autoplay:true,
      autoplayTimeout:7000,
      autoplayHoverPause:false,
      responsiveClass:true,
      responsive:{
          0:{
              items:1,
          },
          600:{
              items:1,
          },
          1000:{
              items:2,
          }
      }

    })

    /****************************
      Active selecty
    *****************************/

    $('.selecty').selecty();

    /****************************
      Active selecty
    *****************************/

    $("#sd-share").click(function () {
      $(".social_share").slideToggle('slide');
    });     

    /******************
      Magnify POP UP
    ********************/

    // Initialize Magnific Popup Gallery + Options
    $('.galary_img').each(function () {
        $(this).magnificPopup({
            delegate: 'a',
            gallery: {
                enabled: true
            },
            type: 'image'
        });
    }); 

    /******************
      NewsLetter POP UP
    ********************/

      setTimeout(function(){
         $('#myModal').modal('show');
     }, 2000);
      
		
	});// DOM Ready

}(jQuery)); // IIFE
